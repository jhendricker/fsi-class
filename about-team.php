<?php
/**
 * Template Name: About Team Template
 *
 *
 * @package FSI-CLASS
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<div class="content-container">
				<h1 class="entry-title"><?php wp_title(''); ?></h1>
				
  				<div class="list about-team">
					<?php
					    $args = array(
					      'post_type' => 'team',
					    );
					    $tests = new WP_Query( $args );
					    if( $tests->have_posts() ) {
					      while( $tests->have_posts() ) {
					        $tests->the_post();
					        ?>
						        <?php if( get_field('link') ): ?>
									<a href="<?php the_field('link'); ?>">
								<?php endif; ?>
						        <div class="item">
									<img src="<?php if ( get_field('picture') ) :
										print get_field('picture');
									endif; ?>
									">
									<h2><?php if ( get_field('name') ) :
										print get_field('name');
									endif; ?></h2>
									<h3><?php if ( get_field('position') ) :
										print get_field('position');
									endif; ?></h3>
									<?php if ( get_field('description') ) :
										print get_field('description');
									endif; ?>
									<div class="quote">
										<?php if ( get_field('quote') ) :
											print get_field('quote');
										endif; ?>
										<div class="author">
											<?php if ( get_field('author') ) :
												print get_field('author');
											endif; ?>
										</div>
									</div>
									<?php if( get_field('link') ): ?>
										<div class="item-footer">
											<button class="second-button">Learn More <i class="fa fa-chevron-right"></i></button>
										</div>
									<?php endif; ?>
								</div>
								<?php if( get_field('link') ): ?>
									</a>
								<?php endif; ?>
					        <?php
					      }
					    }
					    else {
					      echo 'Oh ohm no tests!';
					    }
					  ?>
				</div>
			</div>
		</main><!-- #main -->
	</div><!-- #primary -->

	<script type="text/javascript" src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/jquery.easing.1.3.js"></script>
		<!-- the jScrollPane script -->
		<script type="text/javascript" src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/jquery.mousewheel.js"></script>
	<script type="text/javascript" src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/jquery.contentcarousel.js"></script>
		<script type="text/javascript">
			$('#ca-container').contentcarousel();
		</script>

<?php
get_footer();
