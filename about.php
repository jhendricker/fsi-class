<?php
/**
 * Template Name: About Page Template
 *
 *
 * @package FSI-CLASS
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<div class="content-container">
				<h1 class="entry-title"><?php wp_title(''); ?></h1>
				<h2>Motivation, Objectives, Collaborating Institutes</h2>

				<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/venn.png" class="image-left">US Space Policy program has turned the nation’s attention toward extending the human presence in the solar system through the exploration of the Moon, Near-Earth Asteroids (NEAs), and the moons of Mars. This exploration program will require scientific activities that address fundamental questions about the history of Earth, the solar system and the universe, as well as supporting exploration technology and engineering that to reduce the risk and increase the productivity of future missions to Mars and beyond

				<br><br>Planetary surfaces are where science and exploration intersect. The Center for Lunar and Asteroid Surface Science (CLASS) node of the NASA Solar System Exploration Research Virtual Institute (SSERVI) facilitates NASA’s exploration of deep space by focusing its goals at the intersection of surface science and surface exploration of rocky, atmosphereless bodies (Figure 2.1). CLASS seeks to further understand the formation and evolution of the surfaces of these bodies so that future exploration missions, whether human or robotic, will be properly equipped to handle any challenges they face.

				<br><br>CLASS at the intersection of exploration and science

				<br><br>A key challenge for exploration is the variety of processes that create, evolve, and shape the surfaces of these targets. CLASS attacks this problem by matching the wide range of processes with a wide range of disciplines, taking an interdisciplinary and synergistic approach to the goal of enabling safe and effective exploration.

				<br><br>The objective of CLASS is to strongly support SMD and HEOMD exploration goals by creating a vibrant and growing critical mass of researchers that can provide scientific synergy with and problem-solving expertise for NASA’s exploration programs. Taking full advantage of the collaborative SSERVI structure, CLASS is, by design, diverse; but this diversity is the intelligential fuel for the cross-feed of ideas and the synergy to create new solutions to exploration challenges. CLASS is truly greater than the sum of its parts and represents a dynamic resource for furthering NASA’s science and exploration goals.

				<br><br>CLASS represents a uniquely coordinated, systematic approach to small body and lunar surface science. Physical properties, microgravity, granular mechanics, impacts, orbital dynamics, space weathering, regoliths, and observations are all pieces of the puzzle we seek to solve. CLASS is designed to bring these pieces together through a research program where each area is mutually supportive, so that theory feeds into experiments, which feeds into observations, which feeds back into better theoretical understanding and new testable hypotheses—all to advance NASA’s SMD science goals and HEOMD exploration objectives. CLASS will take full advantage of the SSERVI structure by assembling the diverse specialists necessary to understand atmosphereless body surface science.

				<br><br>The CLASS model is to use its diverse group of investigators to view science issues from a broad range of perspectives and a very diverse knowledge base. CLASS is a structure where exploration problems can be addressed, ideas tried out, discussed by a knowledgeable group that represent almost every relevant specialization in planetary science, and research advice can be obtained from some of the foremost experts in the world. Experience has shown that this kind of interdisciplinary vetting of ideas has catalyzed a number of fruitful areas of research and energized researchers to try novel approaches to old problems. CLASS is designed to be a vehicle for capturing the dynamic cross-feed of ideas between specializations in support of exploration. This is just the kind of structure and interdisciplinary expertise necessary to address the science and technical issues raised by exploration scenarios such as the Asteroid Retrieval Mission (ARM) concept to capture and return a small asteroid to near-Earth space.

				<br><br>The Moon, Near-Earth Asteroids (NEAs), and the moons of Mars are prime targets for exploration and many members of the space community are interested in the potential of In-Situ Resource Utilization (ISRU). ISRU is the process of taking the natural resources of space, such as mineral rich lunar regolith or the water ice contained in asteroids, and manufacturing them into resources that can be utilized to enable further exploration. The potential for ISRU is tremendous, and the development of technologies that can successfully harvest space resources can lead to space missions that are exponentially cheaper. If the resources of space are to ever be utilized in this manner, then the environments that they reside in must first be understood—and that is where the research objectives of CLASS are focused.

				<br><br>These CLASS research areas are: Physical Properties of Exploration Targets, Regoliths, Impacts and Dynamics, and Observations of Small Bodies. Moreover, these focus areas were chosen specifically to leverage projects and facilities at the University of Central Florida (UCF), the Florida Space Institute (FSI), Kennedy Space Center (KSC), and Marshall Space Flight Center (MSFC). CLASS will take advantage of the synergies between NASA-KSC and NASA-MSFC, UCF and the University of Florida (UF), and our network of outstanding international researchers.

				<h2>Collaborator Network</h2>

				The CLASS team is composed of leading planetary scientists, geologists, geochemists, dynamicists, engineers, physicists and other researchers from across the world, and is headed by Prof. Daniel T. Britt at the University of Central Florida. The CLASS network incorporates ?? domestic institutions across the USA and ?? international partner institutions in ?? different countries.

				<br><br>CLASS will take advantage of a dynamic partnership between the NASA centers in the southeast (KSC and MSFC), our core group of scientists at the University of Central Florida (UCF) and the University of Florida (UF), and a wide network of researchers (Tables 5.1 and 7.7) linked together for virtual collaboration and problem-solving. Of particular importance to the CLASS network are our international collaborators who are leading investigators in their areas of specialization. Their active participation in CLASS investigations and institutional programs strongly leverages the work of CLASS co-investigators, broadens the knowledge base, and strongly enhances CLASS research synergy.

			</div>
		</main><!-- #main -->
	</div><!-- #primary -->

	<script type="text/javascript" src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/jquery.easing.1.3.js"></script>
		<!-- the jScrollPane script -->
		<script type="text/javascript" src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/jquery.mousewheel.js"></script>
	<script type="text/javascript" src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/jquery.contentcarousel.js"></script>
		<script type="text/javascript">
			$('#ca-container').contentcarousel();
		</script>

<?php
get_footer();
