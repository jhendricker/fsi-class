<?php
/**
 * Template Name: Contact Us Template
 *
 *
 * @package FSI-CLASS
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<div class="content-container">
				<h1 class="entry-title"><?php wp_title(''); ?></h1>
				<div class="row">
					<div class="col-50">
						<?php if ( get_field('address') ) :
							print get_field('address');
						endif; ?>
					</div>
					<div class="col-50">
						<?php if ( get_field('phone_number') ) :
							print get_field('phone_number');
						endif; ?>
					</div>
				</div>

			</div>
			<div class="map-responsive">
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3503.4570622409183!2d-81.19966684906646!3d28.586062082349958!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x88e7686f918ea233%3A0x30bedf6f4a500dca!2s12354+Research+Pkwy+%23214%2C+Orlando%2C+FL+32826!5e0!3m2!1sen!2sus!4v1460398056934" frameborder="0" style="border:0" allowfullscreen></iframe>
			</div>
		</main><!-- #main -->
	</div><!-- #primary -->

	<script type="text/javascript" src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/jquery.easing.1.3.js"></script>
		<!-- the jScrollPane script -->
		<script type="text/javascript" src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/jquery.mousewheel.js"></script>
	<script type="text/javascript" src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/jquery.contentcarousel.js"></script>
		<script type="text/javascript">
			$('#ca-container').contentcarousel();
		</script>

<?php
get_footer();
