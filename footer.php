<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package FSI-CLASS
 */

?>

	<footer id="colophon" class="site-footer" role="contentinfo">
		<!--<div class="site-info">
			<a href="<?php echo esc_url( __( 'https://wordpress.org/', 'fsi-class' ) ); ?>"><?php printf( esc_html__( 'Proudly powered by %s', 'fsi-class' ), 'WordPress' ); ?></a>
			<span class="sep"> | </span>
			<?php printf( esc_html__( 'Theme: %1$s by %2$s.', 'fsi-class' ), 'fsi-class', '<a href="http://underscores.me/" rel="designer">Underscores.me</a>' ); ?>
		</div>--><!-- .site-info -->
	</footer><!-- #colophon -->
	<?php wp_footer(); ?>
	</div><!-- #content -->

	
</div><!-- #page -->

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-85109144-1', 'auto');
  ga('send', 'pageview');

</script>

</body>
</html>
