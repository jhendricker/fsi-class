<?php
/**
 * Template Name: Index Template
 *
 * Custom template used for index page.
 * All custom fields for this page located under Pages > Index Page
 *
 * @package FSI-CLASS
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php wdp_slider(1); ?>
			
			<!--<div class="slider-container">
				<ul id="slideshow">
			        <li><img src="wp-content/themes/fsi-class/img/starry-shot.jpg" class="img-responsive"></li>
			        <li><img src="wp-content/themes/fsi-class/img/logo-color.jpg" class="img-responsive"></li>
			        <li><img src="wp-content/themes/fsi-class/img/starry-shot.jpg" class="img-responsive"></li>
			    </ul><br clear="all" />
			</div>-->

			<a href="<?php if ( get_field('link_1_link') ) :
				print get_field('link_1_link');
			endif; ?>">
				<div class="link-container" 
				style="background-image:url(<?php if ( get_field('link_1_pic') ) :
					print get_field('link_1_pic');
				endif; ?>);">
					<h2><?php if ( get_field('link_1_title') ) :
						print get_field('link_1_title');
					endif; ?></h2>
					<p><?php if ( get_field('link_1_desc') ) :
						print get_field('link_1_desc');
					endif; ?></p>
					<button class="second-button">Learn More <i class="fa fa-chevron-right"></i></button>
				</div>
			</a>

			<a href="<?php if ( get_field('link_2_link') ) :
				print get_field('link_2_link');
			endif; ?>">
				<div class="link-container center" style="background-image:url(<?php if ( get_field('link_2_pic') ) :
					print get_field('link_2_pic');
				endif; ?>);">
					<h2><?php if ( get_field('link_2_title') ) :
						print get_field('link_2_title');
					endif; ?></h2>
					<p><?php if ( get_field('link_2_desc') ) :
						print get_field('link_2_desc');
					endif; ?></p>
					<button class="second-button">Learn More <i class="fa fa-chevron-right"></i></button>
				</div>
			</a>

			<a href="<?php if ( get_field('link_3_link') ) :
				print get_field('link_3_link');
			endif; ?>">
				<div class="link-container" style="background-image:url(<?php if ( get_field('link_3_pic') ) :
					print get_field('link_3_pic');
				endif; ?>);">
					<h2><?php if ( get_field('link_3_title') ) :
						print get_field('link_3_title');
					endif; ?></h2>
					<p><?php if ( get_field('link_3_desc') ) :
						print get_field('link_3_desc');
					endif; ?></p>
					<button class="second-button">Learn More <i class="fa fa-chevron-right"></i></button>
				</div>
			</a>

			<div class="clear"></div>

			<div class="description-container"
				style="background-image:url(<?php if ( get_field('desc_pic') ) :
					print get_field('desc_pic');
				endif; ?>)">
				<div class="description-content">
					<p class="desc">
						<?php if ( get_field('class_description') ) :
							print get_field('class_description');
						endif; ?>
					</p>
					<a href="about" class="orange-button">Explore <span class="bold">Class <i class="fa fa-angle-double-right"></i></span></a>
				</div>
			</div>

			
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
