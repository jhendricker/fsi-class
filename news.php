<?php
/**
 * Template Name: News and Events
 *
 * 
 *
 * @package FSI-CLASS
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<div class="content-container">
				<h2>News</h2>
				<?php $query = new WP_Query( 'cat=4' ); ?>
				<?php
				if ( $query->have_posts() ) : ?>

					<!--<header class="page-header">
						<?php
							the_archive_title( '<h1 class="page-title">', '</h1>' );
							the_archive_description( '<div class="taxonomy-description">', '</div>' );
						?>
					</header>--><!-- .page-header -->

					<?php
					/* Start the Loop */
					while ( $query->have_posts() ) : $query->the_post();

						/*
						 * Include the Post-Format-specific template for the content.
						 * If you want to override this in a child theme, then include a file
						 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
						 */
						get_template_part( 'template-parts/content', get_post_format() );

					endwhile;

					the_posts_navigation();

				else :

					get_template_part( 'template-parts/content', 'none' );

				endif; ?>

				<h2>Events</h2>
				<?php $query = new WP_Query( 'cat=3' ); ?>
				<?php
				if ( $query->have_posts() ) : ?>

					<!--<header class="page-header">
						<?php
							the_archive_title( '<h1 class="page-title">', '</h1>' );
							the_archive_description( '<div class="taxonomy-description">', '</div>' );
						?>
					</header>--><!-- .page-header -->

					<?php
					/* Start the Loop */
					while ( $query->have_posts() ) : $query->the_post();

						/*
						 * Include the Post-Format-specific template for the content.
						 * If you want to override this in a child theme, then include a file
						 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
						 */
						get_template_part( 'template-parts/content', get_post_format() );

					endwhile;

					the_posts_navigation();

				else :

					get_template_part( 'template-parts/content', 'none' );

				endif; ?>

			</div>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
