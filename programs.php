<?php
/**
 * Template Name: Programs Template
 *
 *
 * @package FSI-CLASS
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<div class="content-container">
				<h1 class="entry-title"><?php wp_title(''); ?></h1>
				The overriding objective of CLASS is to bring a large variety of experienced, multidisciplinary researchers together to cross-feed ideas, new approaches, and new collaborations to address issues in exploration science. To do this, CLASS will implement a series of programs to catalyze interaction and collaboration between CLASS Network members as well as the broader SSERVI and HEOMD communities. These programs include a virtual Colloquium Series, a Directed Discussion Series on exploration science topics, a Student Summer Research Exchange, an annual CLASS Science Conference for face-to-face interaction, and an annual Winter School in Exploration Science.

  				<div class="list">
					<?php
					    $args = array(
					      'post_type' => 'programs',
					    );
					    $tests = new WP_Query( $args );
					    if( $tests->have_posts() ) {
					      while( $tests->have_posts() ) {
					        $tests->the_post();
					        ?>
						        <?php if( get_field('link') ): ?>
									<a href="<?php the_field('link'); ?>">
								<?php endif; ?>
						        <div class="item">
									<img src="<?php if ( get_field('icon') ) :
										print get_field('icon');
									endif; ?>
									">
									<h2><?php if ( get_field('title') ) :
										print get_field('title');
									endif; ?></h2>
									<?php if ( get_field('description') ) :
										print get_field('description');
									endif; ?>
									<?php if( get_field('link') ): ?>
										<div class="item-footer">
											<button class="second-button">Learn More <i class="fa fa-chevron-right"></i></button>
										</div>
									<?php endif; ?>
								</div>
								<?php if( get_field('link') ): ?>
									</a>
								<?php endif; ?>
					        <?php
					      }
					    }
					    else {
					      echo 'Oh ohm no tests!';
					    }
					  ?>
				</div>
			</div>
		</main><!-- #main -->
	</div><!-- #primary -->

	<script type="text/javascript" src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/jquery.easing.1.3.js"></script>
		<!-- the jScrollPane script -->
		<script type="text/javascript" src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/jquery.mousewheel.js"></script>
	<script type="text/javascript" src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/jquery.contentcarousel.js"></script>
		<script type="text/javascript">
			$('#ca-container').contentcarousel();
		</script>

<?php
get_footer();
