<?php
/**
 * Template Name: Block Template
 *
 *
 * @package FSI-CLASS
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<div class="content-container">
				<h1 class="entry-title"><?php wp_title(''); ?></h1>
				The overriding objective of CLASS is to bring a large variety of experienced, multidisciplinary researchers together to cross-feed ideas, new approaches, and new collaborations to address issues in exploration science. To do this, CLASS will implement a series of programs to catalyze interaction and collaboration between CLASS Network members as well as the broader SSERVI and HEOMD communities. These programs include a virtual Colloquium Series, a Directed Discussion Series on exploration science topics, a Student Summer Research Exchange, an annual CLASS Science Conference for face-to-face interaction, and an annual Winter School in Exploration Science.
				
				<div class="list">
					<div class="item">
						<img src="<?php if ( get_field('icon_1') ) :
							print get_field('icon_1');
						endif; ?>
						">
						<h2><?php if ( get_field('title_1') ) :
							print get_field('title_1');
						endif; ?></h2>
						<?php if ( get_field('description_1') ) :
							print get_field('description_1');
						endif; ?>
					</div>
					<div class="item">
						<img src="<?php if ( get_field('icon_2') ) :
							print get_field('icon_2');
						endif; ?>
						">
						<h2><?php if ( get_field('title_2') ) :
							print get_field('title_2');
						endif; ?></h2>
						<?php if ( get_field('description_2') ) :
							print get_field('description_2');
						endif; ?>
					</div>
					<div class="item">
						<img src="<?php if ( get_field('icon_3') ) :
							print get_field('icon_3');
						endif; ?>
						">
						<h2><?php if ( get_field('title_3') ) :
							print get_field('title_3');
						endif; ?></h2>
						<?php if ( get_field('description_3') ) :
							print get_field('description_3');
						endif; ?>
					</div>
					<div class="item">
						<img src="<?php if ( get_field('icon_4') ) :
							print get_field('icon_4');
						endif; ?>
						">
						<h2><?php if ( get_field('title_4') ) :
							print get_field('title_4');
						endif; ?></h2>
						<?php if ( get_field('description_4') ) :
							print get_field('description_4');
						endif; ?>
					</div>
					<div class="item">
						<img src="<?php if ( get_field('icon_5') ) :
							print get_field('icon_5');
						endif; ?>
						">
						<h2><?php if ( get_field('title_5') ) :
							print get_field('title_5');
						endif; ?></h2>
						<?php if ( get_field('description_5') ) :
							print get_field('description_5');
						endif; ?>
					</div>
				</div>

			</div>
		</main><!-- #main -->
	</div><!-- #primary -->

	<script type="text/javascript" src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/jquery.easing.1.3.js"></script>
		<!-- the jScrollPane script -->
		<script type="text/javascript" src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/jquery.mousewheel.js"></script>
	<script type="text/javascript" src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/jquery.contentcarousel.js"></script>
		<script type="text/javascript">
			$('#ca-container').contentcarousel();
		</script>

<?php
get_footer();
