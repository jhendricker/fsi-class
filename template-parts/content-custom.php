<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package FSI-CLASS
 */

?>
<article id="post-<?php the_ID(); ?>" class="post-custom" <?php post_class(); ?>>

	<div class="entry-image">
		<?php if ( has_post_thumbnail() ) {
			the_post_thumbnail();
			} else { ?>
			<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/default.jpg" alt="<?php the_title(); ?>" />
		<?php } ?>

	</div>
	<div class="entry-side">
		<header class="entry-header">
			<?php
				if ( is_single() ) {
					the_title( '<h1 class="entry-title">', '</h1>' );
				} else {
					the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
				}

			if ( 'post' === get_post_type() ) : ?>
			<?php
			endif; ?>
			<a class="second-button" href="<?php echo esc_url( get_permalink() ); ?>">Read More <i class="fa fa-chevron-right"></i></a>
		</header><!-- .entry-header -->

		<!--<div class="entry-content">
			<?php echo substr(strip_tags(get_the_content()),0,200); ?>
			<br><a href="<?php echo get_permalink(); ?>">Read More</a>
			
		</div>--><!-- .entry-content -->
	</div>

</article><!-- #post-## -->
